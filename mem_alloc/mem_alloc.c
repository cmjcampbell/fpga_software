#include <linux/module.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/miscdevice.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

static u32 *MEM;

static int __init mem_alloc_init(void)
{   
    u32 PHYS;

    printk("*** ALLOCATING MEMORY FOR FPGA2HPS SDRAM ***\n");
    
    MEM = kmalloc(sizeof (u32), GFP_KERNEL);
    if (!MEM) {
        printk("*** FAILED TO ALLOCATE MEMORY ***\n");
    } else {
        printk("*** ALLOCATED MEMORY ***\n");
        printk("VIRT ADDRESS: %#p\n", (void *) MEM);
        PHYS = virt_to_phys((void*) MEM);
        printk("PHYS ADDRESS: %#010x\n", PHYS);

	*MEM = 0x48404840;	
    }

    return 0;
}

static void __exit mem_alloc_exit(void)
{
    printk("*** DEALLOCATING MEMORY FOR FPGA2HPS SDRAM ***");
    /* free(MEM); */
}

module_init(mem_alloc_init);
module_exit(mem_alloc_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Christopher Campbell");
MODULE_DESCRIPTION("Memory Allocator for FPGA2HPS SDRAM Testing");
