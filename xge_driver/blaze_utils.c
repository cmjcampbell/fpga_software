/* Blaze XGE mSGDMA Driver
 *
 * Contributors:
 *  Christopher Campbell (cc3769@columbia.edu)
 *
 * Original driver contributed by Altera.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "blaze_xge_main.h"
#include "blaze_utils.h"

void xge_set_bit(void __iomem *ioaddr, size_t offs, u32 bit_mask)
{
    u32 value = csrrd32(ioaddr, offs);
    value |= bit_mask;
    csrwr32(value, ioaddr, offs);
}

void xge_clear_bit(void __iomem *ioaddr, size_t offs, u32 bit_mask)
{
    u32 value = csrrd32(ioaddr, offs);
    value &= ~bit_mask;
    csrwr32(value, ioaddr, offs);
}

int xge_bit_is_set(void __iomem *ioaddr, size_t offs, u32 bit_mask)
{
    u32 value = csrrd32(ioaddr, offs);
    return (value & bit_mask) ? 1 : 0;
}

int xge_bit_is_clear(void __iomem *ioaddr, size_t offs, u32 bit_mask)
{
    u32 value = csrrd32(ioaddr, offs);
    return (value & bit_mask) ? 0 : 1;
}
