/* Blaze XGE mSGDMA Driver
 *
 * Contributors:
 *  Christopher Campbell (cc3769@columbia.edu)
 *
 * Original driver contributed by Altera.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <linux/kernel.h>
#ifndef __BLAZE_UTILS_H__
#define __BLAZE_UTILS_H__

void xge_set_bit(void __iomem *ioaddr, size_t offs, u32 bit_mask);
void xge_clear_bit(void __iomem *ioaddr, size_t offs, u32 bit_mask);
int xge_bit_is_set(void __iomem *ioaddr, size_t offs, u32 bit_mask);
int xge_bit_is_clear(void __iomem *ioaddr, size_t offs, u32 bit_mask);

#endif /* __BLAZE_UTILS_H__*/
